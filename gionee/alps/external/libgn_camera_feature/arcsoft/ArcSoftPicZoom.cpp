/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for night shot.
 *
 * Author : zhangwu
 * Email  : zhangwu@gionee.com
 * Date   : 2013-09-07
 *
 *************************************************************************************/

#define LOG_TAG "ArcSoftPicZoom"
#include <android/log.h>
#include <string.h>

#include <stdlib.h>
#include <stdio.h>

#include "ArcSoftPicZoom.h"

#define MEM_SIZE 150 * 1024 * 1024

namespace android { 

ArcSoftPicZoom::ArcSoftPicZoom()
	: mMem(NULL)
	, mEnhancer(NULL)
	, mImageNum(0)
	, mScale(1.0)
	, mInitialized(false)
	, mImgWidth(0)
	, mImgHeight(0)
	, mCmdType(PICZOOM_CMD_TYPE_NONE)
{
	pthread_mutex_init(&mMutex, NULL);
	pthread_cond_init(&mCond, NULL);

	if(pthread_create(&mTid, NULL, defferedWorkRoutine, (void*)this)) {
		PRINTD("Create thread failed");
	}

}

ArcSoftPicZoom::~ArcSoftPicZoom()
{
	pthread_mutex_lock(&mMutex);
	mCmdType = PICZOOM_CMD_TYPE_EXIT;
	pthread_cond_signal(&mCond);
	pthread_mutex_unlock(&mMutex);
	
	pthread_join(mTid, NULL);
	
	pthread_mutex_destroy(&mMutex);
	pthread_cond_destroy(&mCond);
}

int32 
ArcSoftPicZoom::init() 
{
	MRESULT res = 0; 
	
	memset(&mSrcInput, 0, sizeof(mSrcInput));
	memset(&mDstImg, 0, sizeof(mDstImg));
	memset(&mParam, 0, sizeof(mParam));
	memset(&mSrcImages, 0, sizeof(mSrcImages));

	return res;
}

void  
ArcSoftPicZoom::deinit()
{
	GNCameraFeatureDbgTime dbgTime;
	
	if (mInitialized) {
		ADZ_Uninit(&mEnhancer);
		mInitialized = false;
		dbgTime.print(__func__);
	}

	if(mMem != NULL) {
		free(mMem);
		mMem = NULL;
	}

	if(mDstImg.ppu8Plane[0] != NULL) {
		free(mDstImg.ppu8Plane[0]);
		mDstImg.ppu8Plane[0] = NULL;
	}

	for (int i = 0; i < MAX_PICZOOM_INPUT_IMAGES; i ++) {
		if(mSrcImages[i].ppu8Plane[0] != NULL) {
			free(mSrcImages[i].ppu8Plane[0]);
			mSrcImages[i].ppu8Plane[0] = NULL;
		}
	}

	memset(&mSrcImages, 0, sizeof(mSrcImages));
	mImageNum = 0;
}

int32
ArcSoftPicZoom::initEngine(MInt32 width, MInt32 height, GNImgFormat format)
{
	MInt32 res = 0;
	MInt32 stride = 0;
	MInt32 scanline = 0;
	MInt32 memsize = 0; 
	ASVLOFFSCREEN srcImg;
	GNCameraFeatureDbgTime dbgTime;
	
	if (ALIGN_FORMAT != ALIGN_TO_32) {
		stride = ALIGN_TO_SIZE(width, ALIGN_FORMAT);
		scanline = ALIGN_TO_SIZE(height, ALIGN_FORMAT);
	}

	memset(&srcImg, 0, sizeof(srcImg));
	memset(&mDstImg, 0, sizeof(mDstImg));

	srcImg.i32Width		= stride;
	srcImg.i32Height	= scanline;
	srcImg.pi32Pitch[0]	= stride;
	srcImg.pi32Pitch[1]	= stride;

	mDstImg.i32Width			= stride;
	mDstImg.i32Height			= scanline;
	mDstImg.pi32Pitch[0]		= stride;
	mDstImg.pi32Pitch[1]		= stride;

	switch (format) {
		case GN_IMG_FORMAT_NV21:
			srcImg.u32PixelArrayFormat = ASVL_PAF_NV21;
			mDstImg.u32PixelArrayFormat = ASVL_PAF_NV21;
			memsize 				= mDstImg.pi32Pitch[0] * scanline + mDstImg.pi32Pitch[1] * (scanline/2);
			mDstImg.ppu8Plane[0]	= (MByte*) malloc(memsize);
			mDstImg.ppu8Plane[1] 	= mDstImg.ppu8Plane[0] + mDstImg.pi32Pitch[0] * scanline;
			break;
		case GN_IMG_FORMAT_YUYV:
			//ASVL_PAF_YUYV

			break;
		default:	
			//ASVL_PAF_NV21
			srcImg.u32PixelArrayFormat = ASVL_PAF_NV21;
			mDstImg.u32PixelArrayFormat = ASVL_PAF_NV21;
			memsize 				= mDstImg.pi32Pitch[0] * scanline + mDstImg.pi32Pitch[1] * (scanline/2);
			mDstImg.ppu8Plane[0]	= (MByte*) malloc(memsize);
			mDstImg.ppu8Plane[1] 	= mDstImg.ppu8Plane[0] + mDstImg.pi32Pitch[0] * scanline;
			break;
	}

	res = ADZ_Init(mMem, MEM_SIZE, &srcImg, &mDstImg, &mEnhancer);
	if (res != 0) {
		//free the mDstImg memory
		if(mDstImg.ppu8Plane[0] != NULL) {
			free(mDstImg.ppu8Plane[0]);
			mDstImg.ppu8Plane[0] = NULL;
		}
		
		PRINTD("Failed to initialize Pic Zoom enginer [#%ld].", res);
		return res;
	}

	mInitialized = true;
	
	dbgTime.print(__func__);
	
	return res;
}

int32
ArcSoftPicZoom::getBurstCnt() 
{
	return MAX_PICZOOM_INPUT_IMAGES;
}

int32 
ArcSoftPicZoom::processPicZoom(LPASVLOFFSCREEN param)
{
	MRESULT res	= 0;
	int memsize	= 0;
	int width	= 0;
    int height	= 0;
	MLong recW	= 0;
	MLong recH	= 0;

	if (!mInitialized) {
		PRINTD("[processPicZoom] not enable.");
		return res;
	}

	PRINTD("[processPicZoom] mImageNum = %d", mImageNum);
		
	if (param != NULL && !startEnhance(param)) {
		return res;
	}

	width = param->i32Width;
	height = param->i32Height;

	memsize = getMemSize(param->u32PixelArrayFormat, width, height);
	
	mSrcInput.bDualCam = MFalse;
	mSrcInput.lImgNum = 0;
	
	ADZ_GetDefaultParam(&mParam);
	mParam.lProcessType = ADZ_PROCESS_TYPE_IMAGE;

	for (int i = 0; i < MAX_PICZOOM_INPUT_IMAGES; i ++) {
		mSrcInput.lImgNum ++;
		mSrcInput.pImages[i] = &mSrcImages[i];
		res = ADZ_PreProcess(mEnhancer, &mSrcInput, i, 
	                 &mParam, MNull, MNull);
		if (res != 0) {
			PRINTE("ADZ_PreProcess failed [#%ld].", res);
			deinit();
			return res;
		}
	}
	
	recW = (MLong)(param->i32Width / mScale);
	recH = (MLong)(param->i32Height / mScale);

	recW = (recW >> 1) << 1;
	recH = (recH >> 1) << 1;

	mParam.rtScaleRegion.top 	= ((height - recH) >> 2) << 1;
	mParam.rtScaleRegion.bottom = mParam.rtScaleRegion.top + recH;
	mParam.rtScaleRegion.left 	= (width - recW) >> 2 << 1;
	mParam.rtScaleRegion.right 	= mParam.rtScaleRegion.left + recW;

	res = ADZ_Enhancement(mEnhancer, &mSrcInput, &mDstImg, &mParam, NULL, NULL);
	if (res != 0) {
		PRINTE("Failed to process pic zoom [#%ld].", res);
	} else {
		memcpy(param->ppu8Plane[0], mDstImg.ppu8Plane[0], memsize);
	}

	for (int i = 0; i < MAX_PICZOOM_INPUT_IMAGES; i ++) {
		if(mSrcImages[i].ppu8Plane[0] != NULL) {
			free(mSrcImages[i].ppu8Plane[0]);
			mSrcImages[i].ppu8Plane[0] = NULL;
		}
	}

	MMemSet(&mSrcInput, 0, sizeof(mSrcInput));
	MMemSet(&mSrcImages, 0, sizeof(mSrcImages));

	mImageNum = 0;

	PRINTD("process pic zoom over!");

	return res;
}

int32 
ArcSoftPicZoom::enablePicZoom(PicZoomParam const param)
{
	MRESULT res = 0;
	
	MInt32 width = param.imgParam.width;
	MInt32 height = param.imgParam.height;
	GNImgFormat format = param.imgParam.format;

	if (param.scaleRatio < 1.0) {
		mScale = 1.0;
	} else {
		mScale = param.scaleRatio;
	}

	if (width != mImgWidth || height != mImgHeight || !mInitialized) {
		/************************************************************************* 
		*  Alloc a new memory for mDstImg and invoke the ADZ_Init() when initialize the engin.
		*  If the image sizes changed, we should uninit the engine first.
		*  (1) invoke ADZ_Uninit()
		*  (2) free the mDstImg previous memory
		*************************************************************************/
		if (mInitialized) {
			ADZ_Uninit(&mEnhancer);
			if(mDstImg.ppu8Plane[0] != NULL) {
				free(mDstImg.ppu8Plane[0]);
				mDstImg.ppu8Plane[0] = NULL;
			}

			mInitialized = false;
		}

		mImgWidth = width;
		mImgHeight = height;
		mFormat = format;

		pthread_mutex_lock(&mMutex);
		mCmdType = PICZOOM_CMD_TYPE_INIT;
		pthread_cond_signal(&mCond);
		pthread_mutex_unlock(&mMutex);
	}
		
	mImageNum = 0;

	return res;
}

int32 
ArcSoftPicZoom::disablePicZoom()
{
	MRESULT res = 0;

	pthread_mutex_lock(&mMutex);
	mCmdType = PICZOOM_CMD_TYPE_DEINIT;
	pthread_cond_signal(&mCond);
	pthread_mutex_unlock(&mMutex);

	return res;
}

bool
ArcSoftPicZoom::startEnhance(LPASVLOFFSCREEN param)
{
	bool res = false;
	int memsize = 0;

	memsize = getMemSize(param->u32PixelArrayFormat, param->i32Width, param->i32Height);
	memcpy(&mSrcImages[mImageNum], param, sizeof(ASVLOFFSCREEN));

	if (mImageNum < MAX_PICZOOM_INPUT_IMAGES) {
		mSrcImages[mImageNum].ppu8Plane[0] = (MUInt8*) malloc(memsize);
		if (mSrcImages[mImageNum].ppu8Plane[0] == NULL) {
			PRINTE("Failed to malloc memory.");
		} else {
			memcpy(mSrcImages[mImageNum].ppu8Plane[0], param->ppu8Plane[0], memsize);

			mSrcImages[mImageNum].i32Width  = param->i32Width;
			mSrcImages[mImageNum].i32Height = param->i32Height;
			mSrcImages[mImageNum].pi32Pitch[0] = param->i32Width;
			mSrcImages[mImageNum].pi32Pitch[1] = param->i32Width;

			switch (param->u32PixelArrayFormat) {
				case ASVL_PAF_NV21:
					mSrcImages[mImageNum].ppu8Plane[1] = mSrcImages[mImageNum].ppu8Plane[0] 
													   + mSrcImages[mImageNum].pi32Pitch[0]*param->i32Height;
					//mSrcImages[mImageNum].ppu8Plane[2] = NULL;
					break;
				case ASVL_PAF_YUYV:
					mSrcImages[mImageNum].ppu8Plane[1] = NULL;
					mSrcImages[mImageNum].ppu8Plane[2] = NULL;
					break;
				default:
					//ASVL_PAF_NV21
					mSrcImages[mImageNum].ppu8Plane[1] = mSrcImages[mImageNum].ppu8Plane[0] 
													   + mSrcImages[mImageNum].pi32Pitch[0]*param->i32Height;
					//mSrcImages[mImageNum].ppu8Plane[2] = NULL;
					break;
			}
		}
	}

	mImageNum++;
	if (mImageNum >= MAX_PICZOOM_INPUT_IMAGES) {
		res = true;
	} 

	return res;
}

int32 
ArcSoftPicZoom::getMemSize(MUInt32 u32PixelArrayFormat, int stride, int scanline) 
{
	int memSize = 0;

	switch (u32PixelArrayFormat) {
		case ASVL_PAF_NV21:
			memSize = stride * scanline * 3 / 2;
			break;
		case ASVL_PAF_YUYV:
			memSize = stride * scanline * 2;
			break;
		default:	
			//ASVL_PAF_NV21
			memSize = stride * scanline * 3 / 2;
			break;
	}

	return memSize;
}

void*
ArcSoftPicZoom::
defferedWorkRoutine(void* data)
{
	int running = 1;
	ArcSoftPicZoom* thiz = (ArcSoftPicZoom*)data;

	do {
	  	pthread_mutex_lock(&thiz->mMutex);
    	pthread_cond_wait(&thiz->mCond, &thiz->mMutex);
	  	pthread_mutex_unlock(&thiz->mMutex);

		switch (thiz->mCmdType) {
			case PICZOOM_CMD_TYPE_INIT:
				thiz->initEngine(thiz->mImgWidth, thiz->mImgHeight, thiz->mFormat);
				break;
			case PICZOOM_CMD_TYPE_DEINIT:
				thiz->deinit();
				break;
			case PICZOOM_CMD_TYPE_EXIT:
				running = 0;
				break;
			default:
				break;
		}
	} while(running);
	
	return NULL;
}

};

