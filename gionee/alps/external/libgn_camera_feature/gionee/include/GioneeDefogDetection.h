/*************************************************************************************
 * 
 * Description:
 * 	Defines Gionee APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-03-15
 *
 *************************************************************************************/

#ifndef GIONEE_DEFOG_DETECTION_H_
#define GIONEE_DEFOG_DETECTION_H_

#include <stdlib.h>

#include "AlgImgDefog.h"
#include "GNCameraFeatureDefs.h"
#include "GNCameraFeatureListener.h"
#include "FrameProcessor.h"

namespace android {

class GioneeDefogDetection {
public:
	GioneeDefogDetection();
	~GioneeDefogDetection();

	int32_t init();
	void deinit();

	int32_t setCameraListener(GNCameraFeatureListener* listener);
	int32_t processDefogDetection(PIMGOFFSCREEN imgSrc);
	int32_t enableDefogDetection(int maxWidth, int maxHeight, uint32_t format);
	int32_t disableDefogDetection();
	
private:
	class DefogDetectionHandler: public DefogListener {
		public:
			DefogDetectionHandler(GioneeDefogDetection* thiz) {
				mGioneeDefogDetection = thiz;
			}
			
			virtual void onCallback(int result);
			
		private:
			GioneeDefogDetection* mGioneeDefogDetection;
	};
	
public:
    DefogDetectionHandler* mDefogDetectionHandler;
    FrameProcessor* mFrameProcessor;

	pthread_mutex_t mMutex;
	bool mInitialized;
	bool mDefogDetectEnabled;
	GNCameraFeatureListener* mListener;	
};

};
#endif /*GIONEE_DEFOG_DETECTION_H_*/
